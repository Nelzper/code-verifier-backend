import express, { Express, Request, Response} from "express";
import dotenv from 'dotenv';

//Configuration the .env file
dotenv.config();

//Create Express App
const app: Express = express();
const port: string | number = process.env.PORT || 8000;

//Define the first Route of App
app.get('/', (req: Request, res: Response) => {
    //Send Hello World
    res.send('Welcome to my Api Restful: Express + TS + Nodemon + Swagger + Mongoose');
});

//Define the first Route of App
app.get('/hello', (req: Request, res: Response) => {
    //Send Hello World
    res.send('Welcome to Get Route: ¡Hello!');
});



//Execute App and Listen Requests to PORT
app.listen(port, () => console.log(`Express Server: Running at http://localhost:${port}`));
